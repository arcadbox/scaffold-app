# scaffold-app

Modèle de projet d'app Arcad instanciable via l'outil [`scaffold`](https://gitlab.com/wpetit/scaffold).

Par défaut, le projet utilise:

- [ReactJS](https://fr.reactjs.org/)
- [Redux](https://redux.js.org/)
- [Redux Saga](https://redux-saga.js.org/)
- [Bulma](https://bulma.io/documentation/)

## Utilisation

```shell
mkdir my-app                                                # Créer le répertoire de votre nouveau projet d'app Arcad
cd my-app                                                   # Se positionner dans le nouveau répertoire
scaffold from https://gitlab.com/arcadbox/scaffold-app.git  # Générer le projet
```

Le fichier `README.md` généré vous indiquera comment démarrer à partir de ces nouvelles sources.