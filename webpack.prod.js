const common = require('./webpack.common.js')
const merge = require('webpack-merge')
const path = require('path')
const webpack = require('webpack')

module.exports = [
  merge(common[0], {
    mode: 'production',
    output: {
      path: path.join(__dirname, 'dist/public'),
      publicPath: './',
      filename: '[name].js',
      chunkFilename: '[name].chunk.js',
      sourceMapFilename: "[name].js.map"
    },
    devtool: 'nosources-source-map',
    plugins: [
      new webpack.IgnorePlugin(/redux-logger/),
    ]
  }),
  merge(common[1], {
    mode: 'production',
    output: {
      path: path.join(__dirname, 'dist/backend'),
      publicPath: './',
      filename: 'main.js'
    },
  })
]