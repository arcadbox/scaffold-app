const webpack = require('webpack')
const HTMLPlugin = require('html-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin')
const ImageMinimizerPlugin = require('image-minimizer-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = [
  {
    entry: {
      main: ['./src/frontend/index.tsx'],
    },
    node: {
      fs: 'empty'
    },
    module: {
      rules: [
        {
          test: /\.(t|j)sx?$/,
          use: 'ts-loader',
          exclude: /node_modules/,
        },
        { test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot)(\?.*$|$)/, use: ['file-loader'] },
        { 
          test: /\.(css)$/, 
          use: [
            MiniCssExtractPlugin.loader,
            'css-loader',
            'sass-loader',
            {
              loader: 'postcss-loader',
              options: {
                ident: 'postcss',
                plugins: [
                  require('autoprefixer')(),
                ]
              }
            }
          ]
        },
        { test: /\.ya?ml$/, use: ['json-loader', 'yaml-loader'] }
      ]
    },
    resolve: {
      extensions: ['*',  '.ts', '.tsx', '.js', '.jsx']
    },
    plugins: [
      new MiniCssExtractPlugin(),
      new CleanWebpackPlugin({dir: ['dist/public'] }),
      new webpack.ProvidePlugin({
        'React': 'react'
      }),
      new HTMLPlugin({
        title: 'La Cave',
        hash: true,
        template: 'src/frontend/index.html'
      }),
      new CopyPlugin([
        { from: 'src/arcad.yml', to: '../arcad.yml' }
      ]),
      new ImageMinimizerPlugin({
        minimizerOptions: {
          plugins: [
            ['gifsicle', { interlaced: true }],
            ['mozjpeg', { progressive: true, quality: 70 }],
            ['optipng', { optimizationLevel: 5 }],
            [
              'svgo',
              {
                plugins: [
                  {
                    removeViewBox: false,
                  },
                ],
              },
            ],
          ],
        },
      }),
    ]
  },
  {
    entry: {
      main: ['babel-polyfill', './src/backend/index.ts'],
    },
    devtool: 'inline-source-map',
    node: {
      fs: 'empty'
    },
    module: {
      rules: [
        {
          test: /\.(t|j)sx?$/,
          use: 'ts-loader',
          exclude: /node_modules/,
        },
      ]
    },
    resolve: {
      extensions: ['*', '.ts', '.tsx', '.js', '.jsx']
    },
    plugins: [
      new CleanWebpackPlugin({dir: ['dist/backend'] }),
      new CopyPlugin([
        { from: 'src/arcad.yml', to: '../arcad.yml' }
      ]),
    ]
  }
]