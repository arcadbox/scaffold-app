declare namespace rpc {
    function register(funcName: string): void;
}

declare namespace store {
    function query(collection: string, query?: any, options?: any): [any];
    function save(collection: string, obj: any): any;
    function get(collection: string, objectId: string): any;
    function _delete(collection: string, objectId: string): any;
    export { _delete as delete, query, save, get };
}

declare namespace authorization {
    function isAdmin(userId: string): bool;
}

declare namespace user {
    function getUserById(userId: string): any?;
}