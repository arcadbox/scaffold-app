export function onInit() {
    // List of function exposed to client
    const exposedFuncs = [
        // Setting API
        "findSetting",
        "getSetting",
        "saveSetting",

        // User API
        "getCurrentUserInfo"
    ];
    
    exposedFuncs.forEach(funcName => rpc.register(funcName));
};


export function onUserMessage() {}

export * from "./user";
export * from "./settings";