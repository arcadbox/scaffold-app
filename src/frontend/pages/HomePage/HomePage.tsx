import React, { CSSProperties, Fragment, ReactNode } from 'react';
import { useSelector } from 'react-redux';
import { createSelector } from 'reselect';
import { Settings } from '../../../common/models/setting';
import { AdminButton } from '../../components/AdminButton';
import { AppHeader } from '../../components/AppHeader';
import { RootState } from '../../reducers/root';
import { selectIsAdmin } from '../../selectors/currentUser';
import { useTitle } from '../../hooks/useTitle';

const styles: { [name: string]: CSSProperties } = {
  loader: {
    marginTop: '10%',
  },
};

const createSettingSelector = (id: string) => {
  return (state: RootState) => state.settings.byId[id];
};

const homePageSelector = createSelector(
  createSettingSelector(Settings.BannerTitle),
  selectIsAdmin,
  (bannerTitle, isAdmin) => {
    return {
      bannerTitle: bannerTitle ? bannerTitle.value : "",
      isAdmin,
    }
  }
);

export function HomePage() {
  const { isAdmin, bannerTitle } = useSelector(homePageSelector);

  useTitle(bannerTitle);

  const buttons: JSX.Element[] = [];
  
  if (isAdmin) {
    buttons.push(<AdminButton key="admin-link" to="/admin" />);
  }

  return (
    <Fragment>
      <AppHeader
        title={bannerTitle}
        actionButton={<div className="buttons is-right level-item">{...buttons}</div>} />
      <section className="section">
        <div className="container">
          <p>Bienvenue sur votre nouvelle application !</p>
        </div>
      </section>
    </Fragment>
  );
};

export default HomePage;