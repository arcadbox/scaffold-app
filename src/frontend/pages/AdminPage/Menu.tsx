import React, { FunctionComponent } from "react";
import { Link } from "react-router-dom";

export interface MenuProps {
    selectedPage?: string
};

export const Menu:FunctionComponent<MenuProps> = ({ selectedPage }) => {
    return (
        <aside className="menu">
            <p className="menu-label">
                Administration
            </p>
            <ul className="menu-list">
                <li>
                    <Link to="/admin/settings"
                        className={`${selectedPage === '/admin/settings' ? 'is-active' : ''}`}>
                        Paramètres
                    </Link>
                </li>
            </ul>
        </aside>
    );
};