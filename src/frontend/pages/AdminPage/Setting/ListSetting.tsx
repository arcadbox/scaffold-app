import React, { ChangeEvent, FunctionComponent, useCallback, useEffect, useState } from "react";
import { SettingDefinition } from "../../../../common/models/setting";

export interface ListSettingProps {
    value: any
    onChange: (value: any) => void
    def: SettingDefinition
}

export const ListSetting:FunctionComponent<ListSettingProps> = ({ value, onChange, def }) => {
    const [ state, setState ] = useState({ value });

    const onValueChange = useCallback((evt: ChangeEvent<HTMLSelectElement>) => {
        const value = evt.target.value;
        setState(state => ({ ...state, value }));
    }, [setState]);

    useEffect(() => {
        onChange(state.value);
    }, [state.value]);


    useEffect(() => {
        setState(state => ({ ...state, value }));
    }, [value]);

    return (
        <div className="field">
            <label className="label">{def.label}</label>
            <div className="control">
                <div className="select is-fullwidth">
                    <select value={value} onChange={onValueChange}>
                        {
                            (def.meta || {options:[]}).options.map(opt => {
                                return (
                                    <option key={opt.value}>{opt.label}</option>
                                );
                            })
                        }
                    </select>
                </div>
                <p className="help">{def.description}</p>
            </div>
        </div>
    );
}