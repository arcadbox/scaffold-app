import React from 'react';
import { Link } from 'react-router-dom';

export function NotFoundPage() {
  return (
    <div className="container">
        <div className="columns">
            <div className="column is-6 is-offset-3">
                <div className="message is-warning mt-5">
                   <div className="message-body">
                        <h4 className="is-size-4">Page introuvable !</h4>
                        <p>
                            <Link to="/">Retourner à la page d'accueil</Link>
                        </p>
                   </div>
                </div>
            </div>
        </div>
    </div>
  );
};

export default NotFoundPage;