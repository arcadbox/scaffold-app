import React, { Suspense, useEffect } from "react";
import { useDispatch } from "react-redux";
import {
  HashRouter as Router,
  Route,
  Redirect
} from "react-router-dom";
import { getCurrentUserInfo } from "./actions/rpc";
import { Loader } from "./components/Loader";
import { AnimatedSwitch } from 'react-router-transition';

const LazyHomePage = React.lazy(() => import(/* webpackChunkName: "HomePage" */"./pages/HomePage/HomePage"));
const LazyNotFoundPage = React.lazy(() => import(/* webpackChunkName: "NotFoundPage" */"./pages/NotFoundPage/NotFoundPage"));

const LazySettingList = React.lazy(() => import(/* webpackChunkName: "SettingList" */"./pages/AdminPage/Setting/SettingList"));

export function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCurrentUserInfo());
  }, []);

  return (
    <Suspense fallback={<Loader />}>
      <Router>
        <AnimatedSwitch
          atEnter={{ opacity: 0 }}
          atLeave={{ opacity: 0 }}
          atActive={{ opacity: 1 }}
          className="switch-wrapper"
        >
          <Route path="/" exact component={LazyHomePage} />

          <Route path="/admin" exact component={() => <Redirect to="/admin/settings" />} />
          <Route path="/admin/settings" exact component={LazySettingList} />

          <Route component={LazyNotFoundPage} />
        </AnimatedSwitch>
      </Router>
    </Suspense>
  )
};