declare namespace Arcad {
    function fileUrl(fileId: string): string;
    function upload(data: Blob, metadata?: any): Promise<string>
}