import React, { FunctionComponent } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome } from '@fortawesome/free-solid-svg-icons'
import { Link } from "react-router-dom";

export interface HomeButtonProps {
    className?: string
}

export const HomeButton: FunctionComponent<HomeButtonProps> = ({ className }) => {
    return (
        <Link to="/" className={`button ${className}`}>
            <FontAwesomeIcon icon={faHome} />
        </Link>
    )
};