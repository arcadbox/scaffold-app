import React, { FunctionComponent } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHammer } from '@fortawesome/free-solid-svg-icons'
import { Link } from "react-router-dom";

export interface AdminButtonProps {
    className?: string
    to?: string
}

export const AdminButton: FunctionComponent<AdminButtonProps> = ({ className, to }) => {
    return (
        <Link to={to || '/admin'} className={`button is-link ${className}`}>
            <FontAwesomeIcon icon={faHammer} />
        </Link>
    )
};