import React, { CSSProperties, FunctionComponent } from "react";

export interface LoaderProps {
    style?: CSSProperties
}

export const Loader:FunctionComponent<LoaderProps> = ({ style }) => {
    return null;
}

export default Loader;