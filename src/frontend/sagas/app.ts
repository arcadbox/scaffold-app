import { all, put, race, take, takeLatest } from "redux-saga/effects";
import { AvailableSettings } from "../../common/models/setting";
import { BACKEND_CHANNEL_CONNECT } from "../actions/backend";
import { find, get } from "../actions/rpc";

export function* appSaga() {
    while (true) {
        yield take(BACKEND_CHANNEL_CONNECT);
        
        // Fetch available settings
        const tasks = AvailableSettings.map(def => {
            return put(get('Setting', def.id));
        });

        yield all(tasks);
    }
}