import { call, put } from "redux-saga/effects";
import { RPC_FAILURE, RPC_SUCCESS } from "../actions/rpc";

const Arcad = (window as any).Arcad;

// Arcad.debug = true;

export function* remoteProcedureCallSaga({ job, method, params }: any) {
    let result;
    try {
        result = yield call(Arcad.invoke, method, params);
    } catch(err) {
        yield put({ type: RPC_FAILURE, job, method, name: method, params, err });
        return;
    }

    yield put({ type: RPC_SUCCESS, job, method, name: method, params, result }); 
}