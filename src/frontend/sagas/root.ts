import { all, takeEvery } from 'redux-saga/effects';
import { RPC_REQUEST } from '../actions/rpc';
import { appSaga } from './app';
import { arcadSaga } from './arcad';
import { failureSaga } from './failure';
import { remoteProcedureCallSaga } from './rpc';

export default function* rootSaga() {
  yield all([
    arcadSaga(),
    appSaga(),
    failureSaga(),
    takeEvery(RPC_REQUEST, remoteProcedureCallSaga),
  ]);
}
