import { RootState } from "../reducers/root";

export const selectIsAdmin = function(state: RootState): boolean {
    return state.currentUser.info.isAdmin;
};