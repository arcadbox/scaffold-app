import React, { useState, useEffect } from 'react';

export function useOrientation() {
  const [ orientation, setOrientation ] = useState(window.orientation);
  const isLandscape = orientation === 90 || orientation === -90;
  
  useEffect(() => {
    const onOrientationChange = () => setOrientation(window.orientation);
    window.addEventListener("orientationchange", onOrientationChange);
    return () => {
      window.removeEventListener("orientationchange", onOrientationChange);
    };
  }, []);

  return [ orientation, isLandscape ];
};