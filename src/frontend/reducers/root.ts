import { currentUserReducer, CurrentUserState } from "./currentUser";
import { rpcReducer, RPCState } from "./rpc";
import { settingReducer, SettingState } from "./settings";

export interface RootState {
  currentUser: CurrentUserState
  rpc: RPCState
  settings: SettingState
}

export default {
  rpc: rpcReducer,
  currentUser: currentUserReducer,
  settings: settingReducer,
};