import { Action } from "redux";
import { User } from "../../common/models/user";
import { RPCAction, RPCSuccessAction, RPC_SUCCESS } from "../actions/rpc";

export interface CurrentUserState {
    info: User,
};

const defaultState = {
    info: {
        id: "",
        nickname: "",
        isAdmin: false,
    }
};

export function currentUserReducer(state = defaultState, action: Action) {
    switch (action.type) {
        case RPC_SUCCESS:
            return handleRPCSuccess(state, action as RPCSuccessAction)
    };

    return state;
}

function handleRPCSuccess(state: CurrentUserState, action: RPCSuccessAction) {
    switch (action.method) {
        case "getCurrentUserInfo":
            return handleGetCurrentUserInfo(state, action);
    }

    return state;
}

function handleGetCurrentUserInfo(state: CurrentUserState, action: RPCSuccessAction) {
    return {
        ...state,
        info: {
            ...action.result,
        },
    }
}