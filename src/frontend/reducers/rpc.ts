import { Action } from "redux";
import { ClearRPCStateAction, CLEAR_RPC_STATE, RPCAction, RPC_FAILURE, RPC_REQUEST, RPC_SUCCESS } from "../actions/rpc";

export enum JobState {
    Running,
    Successful,
    Failed,
}

export interface JobStatus {
    state: JobState
}

export interface RPCState {
    jobs: { [jobId: string]: JobStatus }
};

const defaultState = {
    jobs: {}
};

export function rpcReducer(state = defaultState, action: Action) {
    switch(action.type) {
        case RPC_REQUEST:
            return handleRPCRequest(state, action as RPCAction);
        case RPC_FAILURE:
            return handleRPCFailure(state, action as RPCAction);
        case RPC_SUCCESS:
            return handleRPCSuccess(state, action as RPCAction);
        case CLEAR_RPC_STATE:
            return handleClearRPCState(state, action as ClearRPCStateAction);
    }

    return state;
}

function handleRPCRequest(state: RPCState, action: RPCAction) {
    return {
        ...state,
        jobs: {
            ...state.jobs,
            [action.job]: {
                state: JobState.Running,
            }
        }
    }
}

function handleRPCFailure(state: RPCState, action: RPCAction) {
    return {
        ...state,
        jobs: {
            ...state.jobs,
            [action.job]: {
                state: JobState.Failed,
            }
        }
    }
}

function handleRPCSuccess(state: RPCState, action: RPCAction) {
    return {
        ...state,
        jobs: {
            ...state.jobs,
            [action.job]: {
                state: JobState.Successful,
            }
        }
    }
}

function handleClearRPCState(state: RPCState, action: ClearRPCStateAction) {
    const jobs = { ...state.jobs };
    delete jobs[action.job];
    return {
        ...state,
        jobs
    }
}